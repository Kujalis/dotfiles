import subprocess

class Py3status:

    def power_usage(self):
        usage_bytes = subprocess.check_output('cat /sys/class/power_supply/BAT0/power_now',
                                              shell=True)
        return {
            'full_text': usage_bytes.decode(),
            'cached_until': 10,
        }
